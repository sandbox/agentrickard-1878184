<?php

/**
 * @file workbench_access_view.theme.inc
 * Theme functions for workbench_access_view administration pages and forms.
 */

function template_preprocess_workbench_access_view_admin(&$variables) {
  if (!empty($variables['rows'])) {
    $variables['body'] = theme('table', array(
      'header' => $variables['header'],
      'rows' => $variables['rows'],
    ));
  }

  $variables['pager'] = theme('pager');
}

function theme_workbench_access_view_admin($variables) {
  $output = '';

  if ($variables['title']) {
    $output .= "<h2>{$variables['title']}</h2>";
  }
  if ($variables['intro']) {
    $output .= "<p>{$variables['intro']}</p>";
  }

  $output .= $variables['body'];
  $output .= $variables['pager'];

  return $output;
}

function theme_workbench_access_view_admin_form_table($elements) {
  $element = $elements['table'];
  $rows = array();
  foreach (element_children($element) as $k) {
    $row = array();
    foreach (element_children($element[$k]) as $j) {
      $row[] = drupal_render($element[$k][$j]);
    }
    $rows[] = $row;
  }
  return theme('table', array(
    'header' => $element['#header'],
    'rows' => $rows,
  ));
}
